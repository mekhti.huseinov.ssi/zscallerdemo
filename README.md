# ZscallerDemo
Zscaller demo:

1. Docker example:
these commands should be run from following folders with dokerfiles:
     1. \docker\composetest\code\
     2. \docker\dotnetconf\
     3. \docker\webRequests\   (this one runs http requests from within the 
        container)
        
```
        docker-compose build --no-cache
        docker-compose up
        docker-compose down --remove-orphans --rmi local
```

        
2. npm: Angular cli
```
        npm install -g @angular/cli
        npm uninstall -g @angular/cli
        npm cache clean --force
```

3. pip:
```
        pip install matplotlib 
        pip uninstall matplotlib 
```

